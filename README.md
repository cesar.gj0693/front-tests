#  Parte 1

####  Construye un Giphy client que busque perritos, de preferencia en Vue.js 

-  Responsivo.

-  Con componentes reutilizables.

-  Maneja animaciones que le den ganas al usuario de regresar.

-  Guardar Mis búsquedas anteriores en localstorage.

-  Guardar Mis favoritos en localstorage.

Opcionalmente:

-  Usa tailwind para definir los estilos de tu componentes.


#  Parte 2

####  Antes de comenzar esta sección, sigue y lee con atención las siguientes instrucciones:

 
1.  Ve a https://sportdataapi.com/ (API de deportes) y crea una cuenta **gratuita** en https://app.sportdataapi.com/register/?plan=free. No necesitarás ningún método de pago para continuar. 
2. Verifica tu cuenta de correo electrónico. 
3. El plan gratuito solo permite seguir dos ligas, así que escoge las dos siguientes **World Cup** y **World Cup Qualification Women** en el apartado **World**.  Guarda los cambios, y continúa. 
4. Explora la plataforma, lee la documentación y asegúrate de entender el método de autenticación para utilizar el API. 

#### Realiza una tabla que muestre todos los equipos participantes por país. 
1. Utiliza el endpoint `/soccer/countries` para obtener la lista con `name`
y `id` de los países. 
2. Consume el endpoint `/soccer/teams` para obtener la lista de los equipos por país. 
3. Construye una tabla que muestre el `id`, `name` y `logo` de equipo. 
4. Permite el filtrado por país. 
*Extra: Permite la búsqueda por nombre de equipo. :)*

#### Realiza una tabla que muestre los partidos llevados a acabo durante las temporadas. 
1. Las ligas a las que te suscribiste **World Cup** y **World Cup Qualification Women** tienen como id 788 y 782 respectivamente. (puedes consultar sus ids en el endpoint `/soccer/leagues` de igual forma) Utiliza esos ids para obtener de las temporadas de ambas ligas utilizando el endpoint `/soccer/seasons`
2. Utiliza el endpoint `/soccer/matches` para obtener todos los partidos que se han llevado acabo (o llevarán) en *ambas* ligas y muestra en una tabla la fecha del partido, los equipos contendientes, el resultado del partido, el status y el stage del partido. Toma en cuenta la información de los todos los mundiales. 
3. Debe haber elementos que a simple vista sugieran cuál equipo fue el ganador. 
4. Utiliza por default todos los partidos pero haz un componente que permita el filtrado con un rango de fechas con un datepicker. 


#### Consideraciones:  
1. Todos los elementos del proyecto deben ser accesibles durante la navegación, utiliza links, un menú, una barra lateral, lo que gustes. 
2. Las instrucciones para levantar el proyecto deben estar especificadas en un README.md. Lo ideal es que no tengamos que escribirte para resolver dudas o para poder revisar el proyecto. 
3. Debes realizarlo en Vue **2.x**
4. Todos los componentes que utilices deben ser de tu autoría. No puedes usar componentes de terceros. 
5. Toda la comunicación que llevaremos será por escrito, y los comentarios que tengamos sobre la prueba las realizaremos directamente en el repositorio. 

Mucha suerte!